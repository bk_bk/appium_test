#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2017-08-07 12:37:49
# @Author  : zhm (2656117195@qq.com)
# @Link    : https://git.oschina.net/bk_bk
# @Version : $Id$

import os
import time
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction

desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['deviceName'] = 'Android Emulator'
desired_caps['appPackage'] = 'com.example.calculator'
desired_caps['appActivity'] = '.CalculatorActivity'
#7.0手機要設置成uiautomator2
#desired_caps['platformVersion'] = '7.0'
#desired_caps['automationName'] = 'uiautomator2'
#https://testerhome.com/topics/8419
# 设置指定ID的手机
# 如果是远程连接就是ip:端口
# 连接手机 adb tcpip 5555
# adb connect 192.168.1.1:5555
# desired_caps['udid'] = '192.168.1.1:5555'
desired_caps['udid'] = '4d00b50cebd6400d'
#连接appium服务，可以在其他机器上启动服务，远程连接这个服务，可以用命令启动改端口
#appium -a 192.168.199.129 -p 4723 -U 4d00b50cebd6400d
#-a 不写就是默认服务器本机ip 一般-p修改端口就好
#启动好服务器之后，别的机器就可以用下面这个命令远程跑脚本
#driver = webdriver.Remote('http://192.168.199.129:4723/wd/hub', desired_caps)
driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
for x in xrange(1, 20):
    print x
    time.sleep(3)
    driver.start_activity("com.example.calculator", ".CalculatorActivity")
    print driver.current_activity
    time.sleep(2)
    button = driver.find_element_by_id("com.example.calculator:id/num9")
    print button
    button.click()
    time.sleep(2)
    button = driver.find_element_by_xpath("//*[@text=1]")
    print button
    button.click()
driver.quit()
